.. _howtoget:

How to get Tezos
================

In this How To we explain how to get up-to-date binaries to run Tezos
for each network.
You can either use the docker images, which is easier, or build from
sources.


Docker images
-------------

The recommended way for running an up-to-date Tezos node is to use the
docker images that are automatically generated from the GitLab
repository and published on `DockerHub
<https://hub.docker.com/r/tezos/tezos/>`_.
The script ``alphanet.sh`` is provided to help download the right
image for each network and run a simple node.
Its only requirement is a working installation of `Docker
<https://www.docker.com/>`__ and docker compose on a machine with
architecture **x86_64**.
Although we only officially support Linux, the script has been tested
with success in the past on windows/mac/linux.

The same script can be used to run Mainnet, Alphanet or Zeronet, it
suffices to rename it as it downloads a different image based on its
name.
For example, to run Alphanet:

::

    wget https://gitlab.com/tezos/tezos/raw/master/scripts/alphanet.sh
    chmod +x alphanet.sh

Alternatively, to run Mainnet:

::

    wget -O mainnet.sh https://gitlab.com/tezos/tezos/raw/master/scripts/alphanet.sh
    chmod +x mainnet.sh

In the following we assume you are running Alphanet.
You are now one step away from a working node:

::

    ./alphanet.sh start

This will download the right docker image for your chosen network,
launch 3 docker containers running the node, the baker and the
endorser.
The first launch might take a few minutes to download the
docker images and synchronize the chain.

Every call to ``alphanet.sh`` will check for updates of the node and
will fail if your node is not up-to-date. For updating the node, simply
run:

::

    ./alphanet.sh restart

If you prefer to temporarily disable automatic updates, you just have to
set an environment variable:

::

    export TEZOS_ALPHANET_DO_NOT_PULL=yes

See ``./alphanet.sh --help`` for more informations about the
script. In particular see ``./alphanet.sh client --help`` or the
:ref:`online manual<client_manual>` for more information about
the client. Every command to the ``tezos-client`` can be
equivalently executed using ``./alphanet.sh client``.

Cloud images
-------------

Tezos have a set of images for alphanet at the most popular cloud providers, such as AWS, GCP and Azure. Tezos node is installed as a set of services inside a VM and can be managed via SystemD controller.
Use ``sudo systemctl start tezos.service`` to start Tezos node. Use ``sudo systemctl start *@tezos.service`` to start accuser, baker and endorser. Also you can specify service name instead of asterisk to launch each particular service accordingly.
Also make sure to allow port ``8732`` to your IP address to be able to communicate with a Tezos node via RPC.

AWS
~~~~~~~~~~~

To get AWS image simply go to your `aws console <https://console.aws.amazon.com/>`__ at `AMI's page <https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Images:visibility=public-images;search=tezos;sort=name>`_. Select Public AMI at the dropdown menu and search for Tezos images. See the example below:

![Example](aws_image_search_example.png)

``#F00`` Warning! Beware of fakes. Make sure AMI is owned by 754058048930 account before launch.

GCP
~~~~~~~~~~~

To create VM from Tezos image on GCP create a custom image based on Tezos image and launch VM from this custom image.

1. To create a custom image go to the ``Compute engine => Images`` and click ``Create image``. Fulfil all the necessary fields. For example set the ``Tezos`` as the image name, select ``Cloud Storage file`` as a Source and specify the following link as the cloud storage file link => tezos/tezos-image.tar.gz. See the picture below.

![Example](gcp_custom_image_creation_example.png)

2. To launch a VM go to ``Compute Engine => VM Instances`` and click create an instance. Change the boot disk to the custom image created at the first step. Adjust ``Machine type`` to fulfil the node technical requirements. Press ``Create`` to complete the process of the VM creation.

Azure
~~~~~~~~~~~

To create a VM on Azure you'll have to complete a following steps:

0. Log into your Azure account and create a storage account to be able to upload VHD in the next step.

1. Copy Tezos image into your storage account using the Azure CLI.

::

   az storage blob copy start --source-uri $URL \
   --connection-string $CONNECTION_STRING \
   --destination-container opsmanager \
   --destination-blob tezos-image.vhd
   

where ``$CONNECTION_STRING`` is connection string for your storage account and $URL is `https://tezosblobstorage.blob.core.windows.net/tezosstoragecontainer/tezos.vhd`

2. Go to `MS Azure Image creation tab <https://portal.azure.com/#create/Microsoft.Image-ARM>`__ and specify uplodaed VHD as the Image Source. See the picture below:

![Example](azure_custom_image_creation_example.png)

3) Use custom Image to bring up your Tezos VM.

Kubernetes
-------------

Ocassinally you may want to launch a huge set of nodes on a Cloud provider (currently AWS, GCP and Azure are supported). Use ``k8s.bash`` script to bring up a required number of nodes. 

::

    wget -O k8s.bash https://gitlab.com/tezos/tezos/raw/master/scripts/k8s.bash
    chmod +x k8s.bash
    #Use ./k8s.bash to get info about supported set of arguments
    ./k8s.bash

Usage example:

::

    #Creates a cluster called ``tezos`` with a Tezos ``alphanet`` node in the ``us-east1`` region on ``GCP`` 
    ./k8s.bash --provider GCP --network alphanet --region us-east1 -c tezos

Build from sources
------------------

**TL;DR**: Typically you want to do:

::

   sudo apt install -y rsync git m4 build-essential patch unzip bubblewrap wget
   wget https://github.com/ocaml/opam/releases/download/2.0.1/opam-2.0.1-x86_64-linux
   sudo cp opam-2.0.1-x86_64-linux /usr/local/bin/opam
   sudo chmod a+x /usr/local/bin/opam
   git clone https://gitlab.com/tezos/tezos.git
   cd tezos
   git checkout alphanet
   opam init --bare
   make build-deps
   eval $(opam env)
   make
   export PATH=~/tezos:$PATH
   source ./src/bin_client/bash-completion.sh
   export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y


Environment
~~~~~~~~~~~

Currently Tezos is being developed for Linux x86_64, mostly for
Debian/Ubuntu and Archlinux.

The following OSes are reported to work:

- macOS/x86_64
- Linux/armv7h (32 bits) (Raspberry Pi3, etc.)
- Linux/aarch64 (64 bits) (Raspberry Pi3, etc.)

A Windows port is feasible and might be developed in the future.

If ``bubblewrap`` is not available in your distribution you can also
skip it and init opam with ``--disable-sandbox``.

Get the sources
~~~~~~~~~~~~~~~

Tezos *git* repository is hosted at `GitLab
<https://gitlab.com/tezos/tezos/>`_. All development happens here. Do
**not** use our `GitHub mirror <https://github.com/tezos/tezos>`_
which we don't use anymore and only mirrors what happens on GitLab.

You also need to **choose the branch** of the network you want to connect
to: *alphanet*, *zeronet* or *mainnet*.

The *master* branch is where code is merged, but there is no test
network using the master branch directly.


Install OPAM
~~~~~~~~~~~~

To compile Tezos, you need the `OPAM <https://opam.ocaml.org/>`__
package manager, version *2.0*. The build script will take
care of setting-up OPAM, download the right version of the OCaml
compiler, and so on.

Use ``opam init --bare`` to avoid compiling the OCaml compiler now: it
will be done in the next step.


Install Tezos dependencies with OPAM
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install the OCaml compiler and the libraries which Tezos depends on:

::

   make build-deps

This command creates a local opam switch ``_opam`` where the right
version of OCaml is compiled and installed (this takes a while but
it's only done once).

After OCaml it will start with Tezos dependencies, OPAM is able to
handle correctly the OCaml libraries but it is not always able to
handle all external C libraries we depend on. On most system, it is
able to suggest a call to the system package manager but it currently
does not handle version check.

Once the dependencies are done we can update opam's environment to
refer to the new switch and compile the project:

::

   eval $(opam env)
   make

Lastly you can also add Tezos binaries to your ``PATH`` variable,
activate bash autocompletion and after reading the Disclaimer a few
hundred times you are allowed to disable it with
``TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y``.

To add the default opam repository at a lower priority (for example to
install merlin or test other opam packages), you can use the following
command:

::

   opam repo add default --rank=-1
